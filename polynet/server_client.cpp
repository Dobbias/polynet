/* Copyright (c) 2017 Tobias Holewa */

#include "include/polynet/server_client.h"

namespace polynet {

ServerClient::ServerClient(uint8_t id, Server* server, ENetPeer* peer) {
  this->Id = id;
  this->server_ = server;
  this->Peer = peer;
}

void ServerClient::SendPacket(std::shared_ptr<Packet> packet) {
  server_->SendPacket(Id, packet);
}

void ServerClient::AcceptConnect() {
  server_->ServerClientIds.push_back(Id);
  server_->ServerClients.insert(std::pair<uint8_t, unique_ptr<ServerClient>>(
      Id, std::unique_ptr<ServerClient>(this)));
}

void ServerClient::DeclineConnect() {
  enet_peer_reset(Peer);
  delete this;
}
}  // namespace polynet
