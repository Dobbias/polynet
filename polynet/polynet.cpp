/* Copyright (c) 2017 Tobias Holewa */

#include "include/polynet/polynet.h"

#include <enet/enet.h>

#include <iostream>

namespace polynet {
bool PolyNet::Init() {
  if (enet_initialize() != 0) {
    std::cerr << "An error occurred initializing ENet." << std::endl;
    return false;
  }

  return true;
}

void PolyNet::Destroy() { atexit(enet_deinitialize); }
}  // namespace polynet
