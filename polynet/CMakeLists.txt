project(polynetLib)
set(CMAKE_CXX_STANDARD 14)

set(SOURCE_FILES client.cpp include/polynet/client.h server.cpp include/polynet/server.h include/polynet/polynet.h polynet.cpp server_client.cpp include/polynet/server_client.h packet.cpp include/polynet/packet.h sim_server.cpp include/polynet/sim_server.h include/polynet/input.h include/polynet/snapshot.h include/polynet/net_object.h)
find_package(ENet REQUIRED)
find_package(Threads REQUIRED)

add_library (polynetLib STATIC ${SOURCE_FILES})

set(LIBS ${LIBS} ${ENet_LIBRARIES} Threads::Threads)
include_directories(${ENet_INCLUDE_DIRS})

target_link_libraries (polynetLib ${LIBS})
