/* Copyright (c) 2017 Tobias Holewa */

#include "include/polynet/packet.h"

namespace polynet {
/*
 * Copyright © 2015 Brian "Beej Jorgensen" Hall
 * http://beej.us/guide/bgnet/examples/ieee754.c
 */

#define pack754_32(f) (pack754((f), 32, 8))
#define pack754_64(f) (pack754((f), 64, 11))
#define unpack754_32(i) (unpack754((i), 32, 8))
#define unpack754_64(i) (unpack754((i), 64, 11))

uint64_t pack754(long double f, unsigned bits, unsigned expbits) {
  long double fnorm;
  int shift;
  long long sign, exp, significand;
  unsigned significandbits = bits - expbits - 1;  // -1 for sign bit

  if (f == 0.0) return 0;  // get this special case out of the way

  // check sign and begin normalization
  if (f < 0) {
    sign = 1;
    fnorm = -f;
  } else {
    sign = 0;
    fnorm = f;
  }

  // get the normalized form of f and track the exponent
  shift = 0;
  while (fnorm >= 2.0) {
    fnorm /= 2.0;
    shift++;
  }
  while (fnorm < 1.0) {
    fnorm *= 2.0;
    shift--;
  }
  fnorm = fnorm - 1.0;

  // calculate the binary form (non-float) of the significand data
  significand = fnorm * ((1LL << significandbits) + 0.5f);

  // get the biased exponent
  exp = shift + ((1 << (expbits - 1)) - 1);  // shift + bias

  // return the final answer
  return (sign << (bits - 1)) | (exp << (bits - expbits - 1)) | significand;
}

long double unpack754(uint64_t i, unsigned bits, unsigned expbits) {
  long double result;
  long long shift;
  unsigned bias;
  unsigned significandbits = bits - expbits - 1;  // -1 for sign bit

  if (i == 0) return 0.0;

  // pull the significand
  result = (i & ((1LL << significandbits) - 1));  // mask
  result /= (1LL << significandbits);             // convert back to float
  result += 1.0f;                                 // add the one back on

  // deal with the exponent
  bias = (1 << (expbits - 1)) - 1;
  shift = ((i >> significandbits) & ((1LL << expbits) - 1)) - bias;
  while (shift > 0) {
    result *= 2.0;
    shift--;
  }
  while (shift < 0) {
    result /= 2.0;
    shift++;
  }

  // sign it
  result *= (i >> (bits - 1)) & 1 ? -1.0 : 1.0;

  return result;
}

/*
** packi16() -- store a 16-bit int into a char buffer (like htons())
*/
void packi16(uint8_t* buf, uint16_t i, int index) {
  buf[index] = i >> 8;
  buf[index + 1] = i;
}

/*
** packi32() -- store a 32-bit int into a char buffer (like htonl())
*/
void packi32(uint8_t* buf, uint32_t i, int index) {
  buf[index] = i >> 24;
  buf[index + 1] = i >> 16;
  buf[index + 2] = i >> 8;
  buf[index + 3] = i;
}

/*
** packi64() -- store a 64-bit int into a char buffer (like htonl())
*/
void packi64(uint8_t* buf, uint64_t i, int index) {
  buf[index] = i >> 56;
  buf[index + 1] = i >> 48;
  buf[index + 2] = i >> 40;
  buf[index + 3] = i >> 32;
  buf[index + 4] = i >> 24;
  buf[index + 5] = i >> 16;
  buf[index + 6] = i >> 8;
  buf[index + 7] = i;
}

/*
** unpacki8() -- unpack a 8-bit int from a char buffer (like ntohs())
*/
int8_t unpacki8(uint8_t* buf, int index) {
  uint8_t i2 = (uint8_t)buf[index];
  int i;

  // change unsigned numbers to signed
  if (i2 <= 0x7fu) {
    i = i2;
  } else {
    i = -1 - (uint8_t)(0xffu - i2);
  }

  return i;
}

/*
** unpacki16() -- unpack a 16-bit int from a char buffer (like ntohs())
*/
int16_t unpacki16(uint8_t* buf, int index) {
  uint16_t i2 = ((uint16_t)buf[index] << 8) | buf[index + 1];
  int i;

  // change unsigned numbers to signed
  if (i2 <= 0x7fffu) {
    i = i2;
  } else {
    i = -1 - (uint16_t)(0xffffu - i2);
  }

  return i;
}

/*
** unpacku16() -- unpack a 16-bit unsigned from a char buffer (like ntohs())
*/
uint16_t unpacku16(uint8_t* buf, int index) {
  return ((uint16_t)buf[index] << 8) | buf[index + 1];
}

/*
** unpacki32() -- unpack a 32-bit int from a char buffer (like ntohl())
*/
int32_t unpacki32(uint8_t* buf, int index) {
  uint32_t i2 = ((uint32_t)buf[index] << 24) |
                ((uint32_t)buf[index + 1] << 16) |
                ((uint32_t)buf[index + 2] << 8) | buf[index + 3];
  int32_t i;

  // change unsigned numbers to signed
  if (i2 <= 0x7fffffffu) {
    i = i2;
  } else {
    i = -1 - (int32_t)(0xffffffffu - i2);
  }

  return i;
}

/*
** unpacku32() -- unpack a 32-bit unsigned from a char buffer (like ntohl())
*/
uint32_t unpacku32(uint8_t* buf, int index) {
  return ((uint32_t)buf[index] << 24) | ((uint32_t)buf[index + 1] << 16) |
         ((uint32_t)buf[index + 2] << 8) | buf[index + 3];
}

/*
** unpacki64() -- unpack a 64-bit int from a char buffer (like ntohl())
*/
int64_t unpacki64(uint8_t* buf, int index) {
  uint64_t i2 =
      ((uint64_t)buf[index] << 56) | ((uint64_t)buf[index + 1] << 48) |
      ((uint64_t)buf[index + 2] << 40) | ((uint64_t)buf[index + 3] << 32) |
      ((uint64_t)buf[index + 4] << 24) | ((uint64_t)buf[index + 5] << 16) |
      ((uint64_t)buf[index + 6] << 8) | buf[index + 7];
  int64_t i;

  // change unsigned numbers to signed
  if (i2 <= 0x7fffffffffffffffu) {
    i = i2;
  } else {
    i = -1 - (int64_t)(0xffffffffffffffffu - i2);
  }

  return i;
}

/*
** unpacku64() -- unpack a 64-bit unsigned from a char buffer (like ntohl())
*/
uint64_t unpacku64(uint8_t* buf, int index) {
  return ((uint64_t)buf[index] << 56) | ((uint64_t)buf[index + 1] << 48) |
         ((uint64_t)buf[index + 2] << 40) | ((uint64_t)buf[index + 3] << 32) |
         ((uint64_t)buf[index + 4] << 24) | ((uint64_t)buf[index + 5] << 16) |
         ((uint64_t)buf[index + 6] << 8) | buf[index + 7];
}

float Packet::d_float() {
  uint32_t unpacked = unpacki32(data_, index_);
  index_ += 4;
  return unpack754_32(unpacked);
}

double Packet::d_double() {
  uint64_t unpacked = unpacki64(data_, index_);
  index_ += 8;
  return unpack754_64(unpacked);
}

int8_t Packet::d_byte() {
  index_ += 1;
  return unpacki8(data_, index_ - 1);
}

int16_t Packet::d_short() {
  index_ += 2;
  return unpacki16(data_, index_ - 2);
}

int32_t Packet::d_int() {
  index_ += 4;
  return unpacki32(data_, index_ - 4);
}

int64_t Packet::d_long() {
  index_ += 8;
  return unpacki64(data_, index_ - 8);
}

string Packet::d_string() {
  int size = 0;
  while ((data_[index_ + size]) != 0) size++;
  std::string result((char*)(data_ + index_), size + 1);
  index_ += result.size();
  return result;
}

bool Packet::d_bool() {
  bool result = (data_[index_] == 0 ? false : true);
  index_++;
  return result;
}

void Packet::s_float(float value) {
  packi32(data_, pack754_32(value), index_);
  index_ += 4;
}

void Packet::s_double(double value) {
  packi64(data_, pack754_64(value), index_);
  index_ += 8;
}

void Packet::s_byte(int8_t value) {
  data_[index_] = value;
  index_ += 1;
}

void Packet::s_short(int16_t value) {
  packi16(data_, value, index_);
  index_ += 2;
}

void Packet::s_int(int32_t value) {
  packi32(data_, value, index_);
  index_ += 4;
}

void Packet::s_long(int64_t value) {
  packi64(data_, value, index_);
  index_ += 8;
}

void Packet::s_string(string value) {
  value.copy((char*)data_ + index_, value.size(), 0);
  data_[index_ + value.size()] = 0;
  index_ += value.size() + 1;
}

void Packet::s_bool(bool value) {
  data_[index_] = value ? 1 : 0;
  index_++;
}

uint8_t* Packet::getBuffer(int size, int& outSize) {
  outSize = size + sizeof(uint16_t);
  uint8_t* buffer = new uint8_t[outSize];
  data_ = buffer;
  index_ = 0;
  s_short(packet_id_);

  return buffer;
}

void Packet::setBuffer(uint8_t* data) {
  // skip packetID
  index_ = 2;
  data_ = data;
}

uint16_t Packet::GetPacketId(uint8_t* data) { return unpacki16(data, 0); }
}  // namespace polynet
