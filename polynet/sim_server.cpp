/* Copyright (c) 2017 Tobias Holewa */

#include "include/polynet/sim_server.h"

namespace polynet {

SimServer::SimServer(bool serverMode, float fineDeltaBetweenInputs) {
  isServer_ = serverMode;
  deltaStepBetweenInputs_ = fineDeltaBetweenInputs;
  isRunning_ = false;
  time_ = 0;
}

void SimServer::OnStart() {
  if (isServer_) {
    shared_ptr<Snapshot> firstSnap = CreateSnapshot();
    server_->CurrentNetId = 0;
    // Send Net_Ids To Clients
    for (auto& x : server_->ServerClients) {
      firstSnap->NetIds.insert(
          std::make_pair(x.second->Id, server_->CurrentNetId));
      // 65536 * 2 should be enough NetId's per client
      server_->CurrentNetId += 65536 * 2;
      x.second->SendPacket(std::make_shared<InitPacket>(x.first));
    }
    firstSnap->Duration = 0;
    sRollbackSnapshots_.push_back(firstSnap);
    currentSnapshot_ = firstSnap;
    sRollbackInputs_.push_back(vector<shared_ptr<Input>>());
  } else {
    cOtherUsersTime_ = 0;
  }
  isRunning_ = true;
}

void SimServer::ProcessInput(shared_ptr<Input> input) {
  cCurrentFrameInputs_.push_back(input);
}

void SimServer::sProcessIncomingInput(shared_ptr<Input> input) {
  sReceivedInputs_.push_back(input);
}

void SimServer::cProcessIncomingSnapshot(shared_ptr<Snapshot> snapshot) {
  snapshot->Inputs.erase(
      std::remove_if(snapshot->Inputs.begin(), snapshot->Inputs.end(),
                     [&](shared_ptr<Input>& element) {
                       return element->ClientId == client_->ClientId;
                     }),
      snapshot->Inputs.end());

  if (cReceivedSnapshots_.size() > 0 &&
      snapshot->TimeStamp == cReceivedSnapshots_.back()->TimeStamp)
    cReceivedSnapshots_.back() = snapshot;
  else
    cReceivedSnapshots_.push_back(snapshot);

  // set cFilledPrefetchBuffer_ if conditions met
  if (!filledPrefetchBuffer_) {
    if (client_->NetworkCacheLength == 0) {
      if (cReceivedSnapshots_.size() == 2) filledPrefetchBuffer_ = true;
    } else {
      if (cReceivedSnapshots_.size() > 1) {
        float timeCached = 0;
        std::for_each(cReceivedSnapshots_.begin(), cReceivedSnapshots_.end(),
                      [&timeCached](const auto& snapshot) {
                        timeCached += snapshot->Duration;
                      });
        if (timeCached >= client_->NetworkCacheLength)
          filledPrefetchBuffer_ = true;
      }
    }
  }

  if (!filledPrefetchBuffer_) {
    if (cReceivedSnapshots_.size() == 1) {
      currentSnapshot_ = cReceivedSnapshots_.at(0);
      cActiveSnapshot_ = getCurrentSnapshotCloned();
    }
  }
}

// Advances snapshot by time using the inputs for the listed clients
void SimServer::multiStepUpdateSnapshot(float delta, vector<uint8_t> clientIds,
                                        vector<shared_ptr<Input>> inputs,
                                        Snapshot* snapshot, bool clientEvents,
                                        bool excludeLocalPlayerClientEvents,
                                        float clientEventsStart,
                                        Snapshot* rewindSnapshot) {
  // remove input from not valid clientids
  inputs.erase(
      std::remove_if(inputs.begin(), inputs.end(),
                     [&](shared_ptr<Input>& element) {
                       return !(std::find(clientIds.begin(), clientIds.end(),
                                          element->ClientId) !=
                                clientIds.end()) ||
                              element->TimeStamp < snapshot->TimeStamp ||
                              element->TimeStamp >= snapshot->TimeStamp + delta;
                     }),
      inputs.end());

  // sort inputs by time
  std::sort(inputs.begin(), inputs.end(),
            [](const shared_ptr<Input>& lhs, const shared_ptr<Input>& rhs) {
              return lhs->TimeStamp < rhs->TimeStamp;
            });

  float currentTime = snapshot->TimeStamp;

  float deltaBetweenInputsProcessed = 0;
  float targetTime;
  float tinyDelta;

  if (inputs.empty()) {
    targetTime = delta;
    while (deltaBetweenInputsProcessed >= 0) {
      if (deltaBetweenInputsProcessed + deltaStepBetweenInputs_ > targetTime) {
        tinyDelta = targetTime - deltaBetweenInputsProcessed;
        deltaBetweenInputsProcessed = -1;
      } else {
        tinyDelta = deltaStepBetweenInputs_;
        deltaBetweenInputsProcessed += tinyDelta;
      }
      updateClientsAndWorld(tinyDelta, clientIds, clientEvents, snapshot,
                            excludeLocalPlayerClientEvents, clientEventsStart,
                            rewindSnapshot);
    }
    return;
  }

  while (!inputs.empty()) {
    auto& input = inputs.at(0);
    float timeBeforeInput = input->TimeStamp - currentTime;

    deltaBetweenInputsProcessed = 0;
    targetTime = timeBeforeInput;
    while (deltaBetweenInputsProcessed >= 0) {
      if (deltaBetweenInputsProcessed + deltaStepBetweenInputs_ > targetTime) {
        tinyDelta = targetTime - deltaBetweenInputsProcessed;
        deltaBetweenInputsProcessed = -1;
      } else {
        tinyDelta = deltaStepBetweenInputs_;
        deltaBetweenInputsProcessed += tinyDelta;
      }
      updateClientsAndWorld(tinyDelta, clientIds, clientEvents, snapshot,
                            excludeLocalPlayerClientEvents, clientEventsStart,
                            rewindSnapshot);
    }

    bool denyClientEvents =
        (clientEventsStart > input->TimeStamp && clientEventsStart >= 0) ||
        (excludeLocalPlayerClientEvents &&
         input->ClientId == client_->ClientId);

    ApplyInput(input.get(), snapshot, denyClientEvents ? false : clientEvents);
    currentTime += timeBeforeInput;
    inputs.erase(inputs.begin());
  }

  deltaBetweenInputsProcessed = 0;
  targetTime = snapshot->TimeStamp + delta - currentTime;
  while (deltaBetweenInputsProcessed >= 0) {
    if (deltaBetweenInputsProcessed + deltaStepBetweenInputs_ > targetTime) {
      tinyDelta = targetTime - deltaBetweenInputsProcessed;
      deltaBetweenInputsProcessed = -1;
    } else {
      tinyDelta = deltaStepBetweenInputs_;
      deltaBetweenInputsProcessed += tinyDelta;
    }
    updateClientsAndWorld(tinyDelta, clientIds, clientEvents, snapshot,
                          excludeLocalPlayerClientEvents, clientEventsStart,
                          rewindSnapshot);
  }
}

void SimServer::updateClientsAndWorld(float delta, vector<uint8_t> clientIds,
                                      bool clientEvents, Snapshot* snapshot,
                                      bool excludeLocalPlayerClientEvents,
                                      float clientEventsStart,
                                      Snapshot* rewindSnapshot) {
  for (const auto& id : clientIds) {
    bool denyClientEvents =
        (clientEventsStart > (snapshot->TimeStamp + delta) &&
         clientEventsStart >= 0) ||
        (excludeLocalPlayerClientEvents && id == client_->ClientId);
    if (denyClientEvents) clientEvents = false;
    UpdateClientAndRelatedObjects(delta, id, clientEvents, snapshot,
                                  rewindSnapshot);
  }
  UpdateSnapshotWithoutClients(delta, clientEvents, snapshot);
}

void SimServer::Update(float delta) {
  time_ += delta;

  if (!isServer_)
    client_->ProcessReceivedENetPackets();
  else {
    server_->ProcessClientConnecting();
    server_->ProcessReceivedENetPackets();
  }

  if (isRunning_) {
    if (!isServer_)
      clientTick(delta);
    else
      serverTick(delta);
  }
}

void SimServer::clientTick(float delta) {
  for (auto& x : cCurrentFrameInputs_) {
    // Set Time & ClientId
    x->ClientId = this->client_->ClientId;
    x->TimeStamp = time_;
    SendInput(shared_ptr<Input>(x));
    cInputCache_.push_back(x);
  }
  cCurrentFrameInputs_.clear();

  vector<shared_ptr<Input>> mergedInputs;

  if (filledPrefetchBuffer_) {
    cOtherUsersTime_ += delta;
    float currentTime = currentSnapshot_->TimeStamp;
    cActiveSnapshot_ = getCurrentSnapshotCloned();

    while (currentTime < cOtherUsersTime_) {
      bool inNextSnap =
          currentSnapshot_->TimeStamp + currentSnapshot_->Duration <
          cOtherUsersTime_;
      bool doNextSnap = inNextSnap && cReceivedSnapshots_.size() > 1;

      if (inNextSnap) {
        // End in next snap
        mergedInputs = mergeInputLists(cInputCache_, currentSnapshot_->Inputs);
        multiStepUpdateSnapshot(
            (currentSnapshot_->TimeStamp + currentSnapshot_->Duration) -
                currentTime,
            GetAllClientIds(cActiveSnapshot_.get()), mergedInputs,
            cActiveSnapshot_.get(), true,
            currentTime > cRemoteClientEventsProcessed_,
            cRemoteClientEventsProcessed_);
        currentTime = currentSnapshot_->TimeStamp + currentSnapshot_->Duration;

        if (doNextSnap) {
          // Save current snap in next snap
          cActiveSnapshot_->TimeStamp = cReceivedSnapshots_.at(1)->TimeStamp;
          cActiveSnapshot_->Duration = cReceivedSnapshots_.at(1)->Duration;
          cActiveSnapshot_->Inputs = cReceivedSnapshots_.at(1)->Inputs;
          cReceivedSnapshots_.erase(cReceivedSnapshots_.begin());
          cReceivedSnapshots_.at(0) = cActiveSnapshot_;
          currentSnapshot_ = cReceivedSnapshots_.at(0);
          cActiveSnapshot_ = getCurrentSnapshotCloned();

          // Delete old inputs
          cInputCache_.erase(
              std::remove_if(cInputCache_.begin(), cInputCache_.end(),
                             [&](shared_ptr<Input>& element) {
                               return element->TimeStamp <
                                      currentSnapshot_->TimeStamp;
                             }),
              cInputCache_.end());

        } else {
          // Quit loop since there is no buffer to display
          currentTime = cOtherUsersTime_;
        }
      } else {
        // End in current snap
        mergedInputs = mergeInputLists(cInputCache_, currentSnapshot_->Inputs);
        multiStepUpdateSnapshot(cOtherUsersTime_ - currentTime,
                                GetAllClientIds(cActiveSnapshot_.get()),
                                mergedInputs, cActiveSnapshot_.get(), true,
                                currentTime > cRemoteClientEventsProcessed_,
                                cRemoteClientEventsProcessed_);
        // Quit loop
        currentTime = cOtherUsersTime_;
      }
    }

    cRemoteClientEventsProcessed_ = cOtherUsersTime_;
  }

  cActiveSnapshot_->TimeStamp = cOtherUsersTime_;

  // Update only local player
  multiStepUpdateSnapshot(
      time_ - cOtherUsersTime_, vector<uint8_t>{client_->ClientId},
      mergedInputs, cActiveSnapshot_.get(), false,
      time_ > cLocalClientEventsProcessed_, cLocalClientEventsProcessed_);
  cLocalClientEventsProcessed_ = time_;
}

void SimServer::sApplyClientView(int snapIndex, uint8_t clientId, float delta,
                                 Snapshot* targetSnap) {
  Snapshot* snap =
      sRollbackSnapshots_[snapIndex - server_->ClientNetworkCacheLength].get();

  vector<shared_ptr<Input>> mergedInputs;

  cOtherUsersTime_ = snap->TimeStamp + delta;
  float currentTime = snap->TimeStamp;
  cActiveSnapshot_ = std::dynamic_pointer_cast<Snapshot>(snap->Clone());

  while (currentTime < cOtherUsersTime_) {
    bool inNextSnap = cActiveSnapshot_->TimeStamp + cActiveSnapshot_->Duration <
                      cOtherUsersTime_;
    bool doNextSnap = inNextSnap && sRollbackSnapshots_.size() > snapIndex + 1;

    if (inNextSnap) {
      // End in next snap
      cInputCache_.clear();
      for (int i = snapIndex - server_->ClientNetworkCacheLength;
           i < sRollbackInputs_.size(); i++) {
        for (auto& x : sRollbackInputs_[i]) {
          if (x->ClientId == clientId) cInputCache_.push_back(x);
        }
      }
      auto tempInputs = cActiveSnapshot_->Inputs;
      tempInputs.erase(std::remove_if(tempInputs.begin(), tempInputs.end(),
                                      [&](shared_ptr<Input>& element) {
                                        return element->ClientId == clientId;
                                      }),
                       tempInputs.end());
      mergedInputs = mergeInputLists(cInputCache_, tempInputs);

      multiStepUpdateSnapshot(
          (cActiveSnapshot_->TimeStamp + cActiveSnapshot_->Duration) -
              currentTime,
          GetAllClientIds(cActiveSnapshot_.get()), mergedInputs,
          cActiveSnapshot_.get(), true);
      currentTime = cActiveSnapshot_->TimeStamp + cActiveSnapshot_->Duration;

      if (doNextSnap) {
        // Save current snap in next snap
        cActiveSnapshot_->TimeStamp =
            sRollbackSnapshots_[snapIndex - server_->ClientNetworkCacheLength +
                                1]
                ->TimeStamp;
        cActiveSnapshot_->Duration =
            sRollbackSnapshots_[snapIndex - server_->ClientNetworkCacheLength +
                                1]
                ->Duration;
        cActiveSnapshot_->Inputs =
            sRollbackSnapshots_[snapIndex - server_->ClientNetworkCacheLength +
                                1]
                ->Inputs;
        cActiveSnapshot_ =
            std::dynamic_pointer_cast<Snapshot>(cActiveSnapshot_->Clone());
      } else {
        // Quit loop since there is no buffer to display
        currentTime = cOtherUsersTime_;
      }
    } else {
      // End in current snap
      cInputCache_.clear();
      for (int i = snapIndex - server_->ClientNetworkCacheLength;
           i < sRollbackInputs_.size(); i++) {
        for (auto& x : sRollbackInputs_[i]) {
          if (x->ClientId == clientId) cInputCache_.push_back(x);
        }
      }
      auto tempInputs = cActiveSnapshot_->Inputs;
      tempInputs.erase(std::remove_if(tempInputs.begin(), tempInputs.end(),
                                      [&](shared_ptr<Input>& element) {
                                        return element->ClientId == clientId;
                                      }),
                       tempInputs.end());
      mergedInputs = mergeInputLists(cInputCache_, tempInputs);
      multiStepUpdateSnapshot(cOtherUsersTime_ - currentTime,
                              GetAllClientIds(cActiveSnapshot_.get()),
                              mergedInputs, cActiveSnapshot_.get(), true);
      // Quit loop
      currentTime = cOtherUsersTime_;
    }
  }

  cActiveSnapshot_->TimeStamp = cOtherUsersTime_;

  // Update only local player

  multiStepUpdateSnapshot((sRollbackSnapshots_[snapIndex]->TimeStamp +
                           sRollbackSnapshots_[snapIndex]->Duration) -
                              cOtherUsersTime_,
                          vector<uint8_t>{clientId}, mergedInputs,
                          cActiveSnapshot_.get(), false);

  multiStepUpdateSnapshot(delta, vector<uint8_t>{clientId}, mergedInputs,
                          cActiveSnapshot_.get(), false, false, -1, targetSnap);
}

void SimServer::serverTick(float delta) {
  // Map all received inputs to according list
  for (auto& x : sReceivedInputs_) {
    if (x->TimeStamp < sRollbackSnapshots_[0]->TimeStamp) continue;
    for (int i = 0; i < sRollbackSnapshots_.size(); ++i) {
      auto currentSnap = sRollbackSnapshots_[i];
      shared_ptr<Snapshot> nextSnap = nullptr;

      if (sRollbackSnapshots_.size() > 1 && i < sRollbackSnapshots_.size() - 1)
        nextSnap = sRollbackSnapshots_[i + 1];

      if (x->TimeStamp >= currentSnap->TimeStamp) {
        if (nextSnap == nullptr) {
          sRollbackInputs_[i].push_back(x);
        } else {
          if (x->TimeStamp < nextSnap->TimeStamp) {
            sRollbackInputs_[i].push_back(x);
          }
        }
      }
    }
  }
  sReceivedInputs_.clear();

  // Reapply inputs to all cached snapshots (late input may change snapshot
  // states)
  currentSnapshot_ =
      std::dynamic_pointer_cast<Snapshot>(sRollbackSnapshots_[0]->Clone());
  for (int i = 0; i < sRollbackSnapshots_.size(); ++i) {
    float endTime = ((sRollbackSnapshots_.size() == 1) ||
                     (i == sRollbackSnapshots_.size() - 1))
                        ? time_
                        : sRollbackSnapshots_[i + 1]->TimeStamp;
    auto inputList = sRollbackInputs_[i];

    if (filledPrefetchBuffer_ && i > server_->ClientNetworkCacheLength + 1) {
      // APPLY WITH REWIND
      // S1 -> S2
      multiStepUpdateSnapshot(endTime - sRollbackSnapshots_[i]->TimeStamp,
                              GetAllClientIds(currentSnapshot_.get()),
                              inputList, currentSnapshot_.get(), false);

      // S2 -> S2' using C1 - CN
      // Update C1 like local client after grabbing local client view
      // Foreach client apply
      for (auto& x : server_->ServerClients) {
        sApplyClientView(i, x.second->Id,
                         endTime - sRollbackSnapshots_[i]->TimeStamp,
                         currentSnapshot_.get());
      }

      currentSnapshot_->TimeStamp +=
          endTime - sRollbackSnapshots_[i]->TimeStamp;
    } else {
      multiStepUpdateSnapshot(endTime - sRollbackSnapshots_[i]->TimeStamp,
                              GetAllClientIds(currentSnapshot_.get()),
                              inputList, currentSnapshot_.get(), false);
      currentSnapshot_->TimeStamp +=
          endTime - sRollbackSnapshots_[i]->TimeStamp;
    }
    if (i < sRollbackSnapshots_.size() - 1)
      sRollbackSnapshots_[i + 1] =
          std::dynamic_pointer_cast<Snapshot>(currentSnapshot_->Clone());
  }
  // EDIT THIS

  // Set previous snapshot duration
  if (sRollbackSnapshots_.size() > 0)
    sRollbackSnapshots_.back()->Duration =
        currentSnapshot_->TimeStamp - sRollbackSnapshots_.back()->TimeStamp;

  // Add new snapshot to list of snaps
  sRollbackSnapshots_.push_back(
      std::dynamic_pointer_cast<Snapshot>(currentSnapshot_->Clone()));
  sRollbackInputs_.push_back(vector<shared_ptr<Input>>());

  // Remove old snapshots if not relevant for late input
  if (sRollbackSnapshots_.size() == sSnapshotHistoryCount + 1) {
    sRollbackSnapshots_.erase(sRollbackSnapshots_.begin());
    sRollbackInputs_.erase(sRollbackInputs_.begin());
  }

  if (!filledPrefetchBuffer_) {
    if (sRollbackSnapshots_.size() >= server_->ClientNetworkCacheLength)
      filledPrefetchBuffer_ = true;
  }

  // Set active server snapshot
  currentSnapshot_ = sRollbackSnapshots_.size() < sSnapshotCountLiveInput
                         ? sRollbackSnapshots_[0]
                         : sRollbackSnapshots_[sRollbackSnapshots_.size() -
                                               sSnapshotCountLiveInput];

  // Set active inputs
  currentSnapshot_->Inputs =
      sRollbackInputs_.size() < sSnapshotCountLiveInput
          ? sRollbackInputs_[0]
          : sRollbackInputs_[sRollbackInputs_.size() - sSnapshotCountLiveInput];

  // Send snapshot to all clients
  for (auto& x : server_->ServerClients) {
    x.second->SendPacket(CreateSnapshotPacket(GetCurrentSnapshot()));
  }
}

void SimServer::SetClient(Client* client) { client_ = client; }

void SimServer::SetServer(Server* server) { server_ = server; }

void SimServer::SendToServer(shared_ptr<Packet> packet) {
  client_->SendPacket(packet);
}

vector<shared_ptr<Input>> SimServer::mergeInputLists(
    vector<shared_ptr<Input>> v1, vector<shared_ptr<Input>> v2) {
  vector<shared_ptr<Input>> result;
  result.reserve(v1.size() + v2.size());
  result.insert(result.end(), v1.begin(), v1.end());
  result.insert(result.end(), v2.begin(), v2.end());
  return result;
}

bool SimServer::GetIsRunning() { return isRunning_; }

// Get current client state
shared_ptr<Snapshot> SimServer::GetCurrentSnapshot() {
  if (server_)
    return currentSnapshot_;
  else
    return cActiveSnapshot_;
}

shared_ptr<Snapshot> SimServer::getCurrentSnapshotCloned() {
  return std::dynamic_pointer_cast<Snapshot>(currentSnapshot_->Clone());
}

uint32_t SimServer::GetNextNetId(uint8_t clientId, Snapshot* snapshot) {
  return snapshot->NetIds[clientId]++;
}
}  // namespace polynet
