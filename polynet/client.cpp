/* Copyright (c) 2017 Tobias Holewa */

#include "include/polynet/client.h"
#include <iostream>

namespace polynet {
Client::Client(uint16_t hostPort, string host, unique_ptr<SimServer> simServer,
               float networkCacheLength) {
  SimServerInstance = std::move(simServer);
  SimServerInstance->SetClient(this);
  client_ = enet_host_create(NULL, 1, 2, 0, 0);

  if (client_ == nullptr) {
    std::cerr << "Error occurred while creating ENet client host." << std::endl;
  }

  hostPort_ = hostPort;
  host_ = host;
  enet_address_set_host(&hostAddress_, host.c_str());
  hostAddress_.port = hostPort;
  serverPeer_ = enet_host_connect(client_, &hostAddress_, 2, 0);

  if (serverPeer_ == nullptr) {
    std::cerr << "No available peers for initiating an ENet connection."
              << std::endl;
  }

  ENetEvent event;

  if (enet_host_service(client_, &event, 5000) > 0 &&
      event.type == ENET_EVENT_TYPE_CONNECT) {
    std::cout << "Connected to " << host_ << ":" << hostPort << "."
              << std::endl;
    enet_host_flush(client_);

    running_ = true;
    networkThread_ = make_unique<thread>(&Client::networkLoop, this);
  } else {
    enet_peer_reset(serverPeer_);
    std::cerr << "Connection to " << host_ << ":" << hostPort << " failed."
              << std::endl;
  }

  NetworkCacheLength = networkCacheLength;
}

Client::~Client() {
  running_ = false;
  networkThread_->join();
  enet_host_destroy(client_);
}

void Client::networkLoop(Client* client) {
  ENetEvent event;

  float delta;
  auto lastTime = steady_clock::now();

  while (client->running_) {
    if (client->useFakeLatency_) {
      auto nowTime = steady_clock::now();
      std::chrono::duration<float> deltaDuration = nowTime - lastTime;
      lastTime = nowTime;
      delta = deltaDuration.count();

      client->mutex_packetsToReceiveDelayed_.lock();
      client->packetsToReceiveDelayed_.erase(
          std::remove_if(client->packetsToReceiveDelayed_.begin(),
                         client->packetsToReceiveDelayed_.end(),
                         [&](pair<ENetPacket*, float>& element) {
                           element.second -= delta;
                           if (element.second < 0) {
                             client->mutex_receivedENetPackets_.lock();
                             client->receivedENetPackets_.push(element.first);
                             client->mutex_receivedENetPackets_.unlock();
                             return true;
                           }
                           return false;
                         }),
          client->packetsToReceiveDelayed_.end());
      client->mutex_packetsToReceiveDelayed_.unlock();

      client->mutex_packetsToSendDelayed_.lock();
      client->mutex_packetsToSend_.lock();
      client->packetsToSendDelayed_.erase(
          std::remove_if(client->packetsToSendDelayed_.begin(),
                         client->packetsToSendDelayed_.end(),
                         [&](pair<shared_ptr<Packet>, float>& element) {
                           element.second -= delta;
                           if (element.second < 0) {
                             client->packetsToSend_.push(element.first);
                             return true;
                           }
                           return false;
                         }),
          client->packetsToSendDelayed_.end());
      client->mutex_packetsToSend_.unlock();
      client->mutex_packetsToSendDelayed_.unlock();
    }

    client->mutex_packetsToSend_.lock();
    while (!client->packetsToSend_.empty()) {
      std::shared_ptr<Packet>& packet = client->packetsToSend_.front();
      int size;
      uint8_t* data = packet->Serialize(size);
      ENetPacket* e_packet =
          enet_packet_create(data, size, ENET_PACKET_FLAG_RELIABLE);
      enet_peer_send(client->serverPeer_, 0, e_packet);
      client->packetsToSend_.pop();
      delete[] data;
    }
    client->mutex_packetsToSend_.unlock();

    while (enet_host_service(client->client_, &event, 1) > 0) {
      switch (event.type) {
        case ENET_EVENT_TYPE_RECEIVE:
          if (!client->useFakeLatency_) {
            client->mutex_receivedENetPackets_.lock();
            client->receivedENetPackets_.push(event.packet);
            client->mutex_receivedENetPackets_.unlock();
          } else {
            client->mutex_packetsToReceiveDelayed_.lock();
            client->packetsToReceiveDelayed_.push_back(
                std::make_pair(event.packet, client->fakeLatency_));
            client->mutex_packetsToReceiveDelayed_.unlock();
          }

          break;
        case ENET_EVENT_TYPE_DISCONNECT:
          break;
      }
    }
  }
}

void Client::SendPacket(shared_ptr<Packet> packet) {
  if (useFakeLatency_) {
    mutex_packetsToSendDelayed_.lock();
    packetsToSendDelayed_.push_back(std::make_pair(packet, fakeLatency_));
    mutex_packetsToSendDelayed_.unlock();
  } else {
    mutex_packetsToSend_.lock();
    packetsToSend_.push(packet);
    mutex_packetsToSend_.unlock();
  }
}

void Client::SetFakeLatency(float value) {
  useFakeLatency_ = true;
  fakeLatency_ = value;
}

void Client::ProcessReceivedENetPackets() {
  mutex_receivedENetPackets_.lock();
  while (!receivedENetPackets_.empty()) {
    auto packet = receivedENetPackets_.front();
    uint16_t packetId = Packet::GetPacketId(packet->data);
    // Hide init packet from PolyNet user, handle it here
    if (packetId == InitPacket::ID) {
      auto initPacket = std::make_shared<polynet::InitPacket>();
      initPacket->Deserialize(packet->data);
      this->ClientId = initPacket->ClientId;
    } else {
      SimServerInstance->OnPacketReceived(packetId, packet->data);
    }
    enet_packet_destroy(packet);
    receivedENetPackets_.pop();
  }
  mutex_receivedENetPackets_.unlock();
}
}  // namespace polynet
