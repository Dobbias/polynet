/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_INCLUDE_POLYNET_SERVER_H_
#define POLYNET_INCLUDE_POLYNET_SERVER_H_

#include <enet/enet.h>

#include <chrono>
#include <map>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include "packet.h"
#include "sim_server.h"

namespace polynet {
using namespace std::chrono;

using std::make_unique;
using std::mutex;
using std::pair;
using std::queue;
using std::shared_ptr;
using std::thread;
using std::unique_ptr;

class ServerClient;
class SimServer;

class Server {
 public:
  float ApproximateDelta;
  float MaxAcceptedInputDelay;
  float MaxLiveInputDelay;
  int ClientNetworkCacheLength;
  uint32_t CurrentNetId;
  unique_ptr<SimServer> SimServerInstance;
  std::map<uint8_t, unique_ptr<ServerClient>> ServerClients;
  std::vector<uint8_t> ServerClientIds;

  explicit Server(uint16_t port, unique_ptr<SimServer> simServer,
                  float approximateDelta, float maxAcceptedInputDelay,
                  float maxLiveInputDelay, float clientNetworkCacheLength);
  ~Server();

  void ProcessReceivedENetPackets();
  void ProcessClientConnecting();

  void SendPacket(uint8_t clientId, shared_ptr<Packet> packet);

  void RunInSeperateThread(float targetDelta);
  void StopSeperateThread();

 private:
  static void networkLoop(Server* server);

  bool running_;
  bool seperateThreadRunning_;

  ENetHost* server_;
  unique_ptr<thread> networkThread_;

  mutex mutex_receivedENetPackets_;
  queue<ENetPacket*> receivedENetPackets_;
  mutex mutex_peersConnecting_;
  queue<ENetPeer*> peersConnecting_;
  mutex mutex_packetsToSend_;
  queue<pair<uint8_t, shared_ptr<Packet>>> packetsToSend_;
};
}  // namespace polynet

#endif  // POLYNET_INCLUDE_POLYNET_SERVER_H_
