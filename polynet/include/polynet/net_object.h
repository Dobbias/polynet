/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_INCLUDE_POLYNET_NET_OBJECT_H_
#define POLYNET_INCLUDE_POLYNET_NET_OBJECT_H_

#include <cstdint>
#include <memory>
#include <shared_mutex>
#include "packet.h"

using std::shared_ptr;

namespace polynet {
class NetObject {
 public:
  uint16_t NetId;
  NetObject(uint16_t id = -1) { this->NetId = id; }
  virtual uint32_t GetNetSize() = 0;
  virtual void Serialize(Packet* packet) = 0;
  virtual void Deserialize(Packet* packet) = 0;
  virtual shared_ptr<NetObject> Clone() = 0;
};
}  // namespace polynet

#endif  // POLYNET_INCLUDE_POLYNET_NET_OBJECT_H_
