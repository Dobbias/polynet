/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_INCLUDE_POLYNET_POLYNET_H_
#define POLYNET_INCLUDE_POLYNET_POLYNET_H_

#include "client.h"
#include "packet.h"
#include "server.h"
#include "server_client.h"
#include "sim_server.h"
#include "snapshot.h"

namespace polynet {
class PolyNet {
 public:
  static bool Init();
  static void Destroy();
};
}  // namespace polynet

#endif  // POLYNET_INCLUDE_POLYNET_POLYNET_H_
