/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_INCLUDE_POLYNET_PACKET_H_
#define POLYNET_INCLUDE_POLYNET_PACKET_H_

#include <cstdint>
#include <string>

namespace polynet {

using std::string;

class Packet {
 public:
  Packet(uint16_t size) : packet_id_(size) {}

  virtual void Deserialize(uint8_t* data) = 0;
  virtual uint8_t* Serialize(int& outSize) = 0;

  static uint16_t GetPacketId(uint8_t* data);

  float d_float();
  double d_double();
  int8_t d_byte();
  int16_t d_short();
  int32_t d_int();
  int64_t d_long();
  string d_string();
  bool d_bool();

  void s_float(float);
  void s_double(double);
  void s_byte(int8_t);
  void s_short(int16_t);
  void s_int(int32_t);
  void s_long(int64_t);
  void s_string(string);
  void s_bool(bool);

 protected:
  uint16_t packet_id_;
  uint8_t* data_;
  int index_;

  uint8_t* getBuffer(int size, int& outSize);
  void setBuffer(uint8_t* data);
};

class InitPacket : public Packet {
 public:
  static const uint16_t ID = 0;

  uint8_t ClientId;

  InitPacket(uint8_t clientId = -1) : Packet(ID) { ClientId = clientId; }

  uint8_t* Serialize(int& outSize) {
    uint8_t* outData = getBuffer(sizeof(int32_t), outSize);
    s_byte(ClientId);
    return outData;
  }

  void Deserialize(uint8_t* data) {
    setBuffer(data);
    ClientId = d_byte();
  }
};
}  // namespace polynet

#endif  // POLYNET_INCLUDE_POLYNET_PACKET_H_
