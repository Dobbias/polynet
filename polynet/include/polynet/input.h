/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_INCLUDE_POLYNET_INPUT_H_
#define POLYNET_INCLUDE_POLYNET_INPUT_H_

#include "net_object.h"

namespace polynet {
class Input : public NetObject {
 public:
  uint16_t PacketId;
  float TimeStamp;
  uint8_t ClientId;

  Input(uint16_t packetId) { this->PacketId = packetId; }
};
}  // namespace polynet

#endif  // POLYNET_INCLUDE_POLYNET_INPUT_H_
