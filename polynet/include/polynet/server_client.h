/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_INCLUDE_POLYNET_SERVER_CLIENT_H_
#define POLYNET_INCLUDE_POLYNET_SERVER_CLIENT_H_

#include <enet/enet.h>
#include <memory>
#include "packet.h"
#include "server.h"

namespace polynet {
class Server;

class ServerClient {
 public:
  ENetPeer* Peer;
  uint8_t Id;

  ServerClient(uint8_t id, Server* server, ENetPeer* peer);
  void SendPacket(std::shared_ptr<Packet> packet);
  void AcceptConnect();
  void DeclineConnect();

 private:
  Server* server_;
};
}  // namespace polynet

#endif  // POLYNET_INCLUDE_POLYNET_SERVER_CLIENT_H_
