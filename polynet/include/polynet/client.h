/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_INCLUDE_POLYNET_CLIENT_H_
#define POLYNET_INCLUDE_POLYNET_CLIENT_H_

#include <enet/enet.h>

#include <algorithm>
#include <chrono>
#include <memory>
#include <mutex>
#include <queue>
#include <string>
#include <thread>

#include "packet.h"
#include "sim_server.h"

namespace polynet {

using std::unique_ptr;
using std::make_unique;
using std::shared_ptr;
using std::string;
using std::thread;
using std::queue;
using std::vector;
using std::pair;
using std::chrono::seconds;
using std::chrono::duration;
using std::chrono::steady_clock;
using std::mutex;

class SimServer;

class Client {
 public:
  float NetworkCacheLength;
  uint32_t CurrentNetId;
  uint8_t ClientId;
  unique_ptr<SimServer> SimServerInstance;

  Client(uint16_t hostPort, string host, unique_ptr<SimServer> simServer,
         float networkChacheLength);
  ~Client();

  void SendPacket(shared_ptr<Packet> packet);
  void SetFakeLatency(float value);
  void ProcessReceivedENetPackets();

 private:
  static void networkLoop(Client* client);

  string host_;
  uint16_t hostPort_;
  bool running_;
  bool useFakeLatency_ = false;
  float fakeLatency_;

  ENetAddress hostAddress_;
  ENetHost* client_;
  ENetPeer* serverPeer_;
  unique_ptr<thread> networkThread_;
  mutex mutex_receivedENetPackets_;
  queue<ENetPacket*> receivedENetPackets_;
  mutex mutex_packetsToSend_;
  queue<shared_ptr<Packet>> packetsToSend_;
  mutex mutex_packetsToSendDelayed_;
  vector<pair<shared_ptr<Packet>, float>> packetsToSendDelayed_;
  mutex mutex_packetsToReceiveDelayed_;
  vector<pair<ENetPacket*, float>> packetsToReceiveDelayed_;
};
}  // namespace polynet

#endif  // POLYNET_INCLUDE_POLYNET_CLIENT_H_
