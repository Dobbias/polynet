/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_INCLUDE_POLYNET_SNAPSHOT_H_
#define POLYNET_INCLUDE_POLYNET_SNAPSHOT_H_

#include <map>
#include <vector>

#include "input.h"
#include "net_object.h"

namespace polynet {
using std::vector;
using std::map;

class Snapshot : public NetObject {
 public:
  float TimeStamp;
  float Duration;
  vector<shared_ptr<Input>> Inputs;
  map<uint8_t, uint32_t> NetIds;
};
}  // namespace polynet

#endif  // POLYNET_INCLUDE_POLYNET_SNAPSHOT_H_
