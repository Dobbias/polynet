/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_INCLUDE_POLYNET_SIM_SERVER_H_
#define POLYNET_INCLUDE_POLYNET_SIM_SERVER_H_

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <vector>
#include "client.h"
#include "input.h"
#include "server_client.h"
#include "snapshot.h"

namespace polynet {

using std::map;
using std::shared_ptr;
using std::vector;

class ServerClient;
class Client;
class Server;

class SimServer {
 public:
  int sSnapshotHistoryCount;
  int sSnapshotCountLiveInput;
  SimServer(bool serverMode, float fineDeltaBetweenInputs);
  void ProcessInput(shared_ptr<Input> input);
  virtual void sOnClientConnected(ServerClient* serverClient) = 0;
  virtual void OnPacketReceived(uint16_t packetId, uint8_t* data) = 0;
  void SetClient(Client* client);
  void SetServer(Server* server);
  void SendToServer(shared_ptr<Packet> packet);
  void OnStart();
  void Update(float delta);
  shared_ptr<Snapshot> GetCurrentSnapshot();
  bool GetIsRunning();

 protected:
  bool isRunning_;
  float time_;
  bool isServer_;
  Client* client_;
  Server* server_;
  map<uint8_t, vector<shared_ptr<Input>>> recvClientInput_;
  map<uint8_t, vector<shared_ptr<Snapshot>>> sendClientSnapshots_;
  virtual void UpdateClientAndRelatedObjects(float delta, uint8_t clientId,
                                             bool clientEvents,
                                             Snapshot* snapshot,
                                             Snapshot* rewindSnapshot) = 0;
  virtual void UpdateSnapshotWithoutClients(float delta, bool clientEvents,
                                            Snapshot* snapshot) = 0;
  virtual void ApplyInput(Input* input, Snapshot* snapshot, bool clientEvents,
                          Snapshot* rewindSnapshot = nullptr) = 0;
  virtual void SendInput(shared_ptr<Input> input) = 0;
  virtual shared_ptr<Snapshot> CreateSnapshot() = 0;
  virtual shared_ptr<Packet> CreateSnapshotPacket(
      shared_ptr<Snapshot> snapshot) = 0;
  void sProcessIncomingInput(shared_ptr<Input> input);
  void cProcessIncomingSnapshot(shared_ptr<Snapshot> snapshot);
  uint32_t GetNextNetId(uint8_t clientId, Snapshot* snapshot);
  virtual vector<uint8_t> GetAllClientIds(Snapshot* snapshot) = 0;

 private:
  float cLocalClientEventsProcessed_, cRemoteClientEventsProcessed_;
  float cOtherUsersTime_;
  float deltaStepBetweenInputs_;
  bool filledPrefetchBuffer_ = false;
  shared_ptr<Snapshot> currentSnapshot_;
  shared_ptr<Snapshot> cActiveSnapshot_;
  vector<shared_ptr<Snapshot>> cReceivedSnapshots_;
  vector<shared_ptr<Input>> cCurrentFrameInputs_;
  vector<shared_ptr<Input>> cInputCache_;

  vector<shared_ptr<Snapshot>> sRollbackSnapshots_;
  vector<vector<shared_ptr<Input>>> sRollbackInputs_;
  vector<shared_ptr<Snapshot>> sSentSnapshots_;
  vector<vector<shared_ptr<Input>>> sSentInputs_;
  vector<shared_ptr<Input>> sReceivedInputs_;

  void updateClientsAndWorld(float delta, vector<uint8_t> clientIds,
                             bool clientEvents, Snapshot* snapshot,
                             bool excludeLocalPlayerClientEvents,
                             float clientEventsStart, Snapshot* rewindSnapshot);
  void multiStepUpdateSnapshot(float delta, vector<uint8_t> clientIds,
                               vector<shared_ptr<Input>> inputs,
                               Snapshot* snapshot, bool clientEvents,
                               bool excludeLocalPlayerClientEvents = false,
                               float clientEventsStart = -1,
                               Snapshot* rewindSnapshot = nullptr);
  void serverTick(float delta);
  void clientTick(float delta);
  void sApplyClientView(int snapIndex, uint8_t clientId, float delta,
                        Snapshot* targetSnap);
  vector<shared_ptr<Input>> mergeInputLists(vector<shared_ptr<Input>> v1,
                                            vector<shared_ptr<Input>> v2);
  shared_ptr<Snapshot> getCurrentSnapshotCloned();
};
}  // namespace polynet

#endif  // POLYNET_INCLUDE_POLYNET_SIM_SERVER_H_
