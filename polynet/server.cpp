/* Copyright (c) 2017 Tobias Holewa */

#include "include/polynet/server.h"
#include <iostream>
#include "include/polynet/server_client.h"

namespace polynet {
Server::Server(uint16_t port, unique_ptr<SimServer> simServer,
               float approximateDelta, float maxAcceptedInputDelay,
               float maxLiveInputDelay, float clientNetworkCacheLength) {
  SimServerInstance = std::move(simServer);
  SimServerInstance->SetServer(this);
  ENetAddress address;
  address.host = ENET_HOST_ANY;
  address.port = port;
  server_ = enet_host_create(&address, 64, 2, 0, 0);
  if (server_ == NULL) {
    std::cerr << "Couldn't create ENet server_." << std::endl;
  }

  this->ApproximateDelta = approximateDelta;
  this->MaxAcceptedInputDelay = maxAcceptedInputDelay;
  this->MaxLiveInputDelay = maxLiveInputDelay;

  SimServerInstance->sSnapshotHistoryCount =
      (int)(this->MaxAcceptedInputDelay / this->ApproximateDelta);
  SimServerInstance->sSnapshotCountLiveInput =
      (int)(this->MaxLiveInputDelay / this->ApproximateDelta);
  ClientNetworkCacheLength =
      (int)(clientNetworkCacheLength / this->ApproximateDelta);

  running_ = true;
  networkThread_ = make_unique<thread>(&Server::networkLoop, this);
}

Server::~Server() {
  running_ = false;
  networkThread_->join();
  enet_host_destroy(server_);
}

void Server::networkLoop(Server* server) {
  ENetEvent event;
  while (server->running_) {
    server->mutex_packetsToSend_.lock();
    while (!server->packetsToSend_.empty()) {
      shared_ptr<Packet>& packet = server->packetsToSend_.front().second;
      int size;
      uint8_t* data = (*packet).Serialize(size);
      ENetPacket* e_packet =
          enet_packet_create(data, size, ENET_PACKET_FLAG_RELIABLE);
      enet_peer_send(
          server->ServerClients.find(server->packetsToSend_.front().first)
              ->second->Peer,
          0, e_packet);
      server->packetsToSend_.pop();
      delete[] data;
    }
    server->mutex_packetsToSend_.unlock();

    while (enet_host_service(server->server_, &event, 1) > 0) {
      switch (event.type) {
        case ENET_EVENT_TYPE_CONNECT:
          server->mutex_peersConnecting_.lock();
          server->peersConnecting_.push(event.peer);
          server->mutex_peersConnecting_.unlock();
          break;
        case ENET_EVENT_TYPE_RECEIVE:
          server->mutex_receivedENetPackets_.lock();
          server->receivedENetPackets_.push(event.packet);
          server->mutex_receivedENetPackets_.unlock();

          break;

        case ENET_EVENT_TYPE_DISCONNECT:
          /* Reset the peer's client information. */
          event.peer->data = NULL;
      }
    }
  }
}

void Server::SendPacket(uint8_t clientId, shared_ptr<Packet> packet) {
  mutex_packetsToSend_.lock();
  packetsToSend_.push(pair<uint8_t, shared_ptr<Packet>>(clientId, packet));
  mutex_packetsToSend_.unlock();
}

void Server::RunInSeperateThread(float targetDelta) {
  seperateThreadRunning_ = true;
  thread loopThread(
      [](float targetDelta, Server* server) {
        auto lastTime = high_resolution_clock::now();
        auto nextTick = lastTime;
        nextTick =
            nextTick + duration_cast<nanoseconds>(duration<float>(targetDelta));
        std::chrono::duration<float> updateDelta(0);

        static constexpr std::chrono::duration<double> MinSleepDuration(0);

        while (server->seperateThreadRunning_) {
          // Sleep target delta
          while (high_resolution_clock::now() < nextTick) {
            std::this_thread::sleep_for(MinSleepDuration);
          }
          // Update timer and update server
          updateDelta = high_resolution_clock::now() - lastTime;
          lastTime = lastTime + duration_cast<nanoseconds>(updateDelta);
          nextTick = nextTick +
                     duration_cast<nanoseconds>(duration<float>(targetDelta));
          server->SimServerInstance->Update(updateDelta.count());
        }
      },
      targetDelta, this);
  loopThread.detach();
}

void Server::StopSeperateThread() { seperateThreadRunning_ = false; }

void Server::ProcessReceivedENetPackets() {
  mutex_receivedENetPackets_.lock();
  while (!receivedENetPackets_.empty()) {
    auto packet = receivedENetPackets_.front();
    SimServerInstance->OnPacketReceived(Packet::GetPacketId(packet->data),
                                        packet->data);
    enet_packet_destroy(packet);
    receivedENetPackets_.pop();
  }
  mutex_receivedENetPackets_.unlock();
}

void Server::ProcessClientConnecting() {
  mutex_peersConnecting_.lock();
  while (!peersConnecting_.empty()) {
    auto peer = peersConnecting_.front();
    SimServerInstance->sOnClientConnected(
        new ServerClient(ServerClients.size(), this, peer));
    peersConnecting_.pop();
  }
  mutex_peersConnecting_.unlock();
}
}  // namespace polynet
