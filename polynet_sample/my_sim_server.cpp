/* Copyright (c) 2017 Tobias Holewa */

#include "my_sim_server.h"
#include "shared.h"

namespace polynet_sample {

MySimServer::MySimServer(bool serverMode, float fineDeltaBetweenInputs)
    : SimServer(serverMode, fineDeltaBetweenInputs) {}

void MySimServer::sOnClientConnected(ServerClient* serverClient) {
  serverClient->AcceptConnect();

  // Start Sample Game
  if (server_->ServerClients.size() == 2) {
    OnStart();
    for (auto const& x : server_->ServerClients) {
      x.second->SendPacket(CreateSnapshotPacket(GetCurrentSnapshot()));
    }
  }
}

void MySimServer::OnPacketReceived(uint16_t packetId, uint8_t* data) {
  if (isServer_) {
    switch (packetId) {
      case KeyboardInputPacket::ID: {
        auto packet = std::make_shared<KeyboardInputPacket>();
        packet->Deserialize(data);
        sProcessIncomingInput(packet->Input);
        break;
      }
    }
  } else {
    switch (packetId) {
      case CustomSnapshotPacket::ID: {
        auto packet = std::make_shared<CustomSnapshotPacket>();
        packet->Deserialize(data);
        float xPos = packet->Snapshot->Player2->PosY;
        cProcessIncomingSnapshot(packet->Snapshot);
        if (!GetIsRunning()) OnStart();
        break;
      }
    }
  }
}

void MySimServer::OnStart() {
  SimServer::OnStart();

  if (isServer_) {
    auto snap = std::dynamic_pointer_cast<CustomSnapshot>(GetCurrentSnapshot());
    snap->Player1->PosX = 0;
    snap->Player1->PosY = (HEIGHT / 2.0f) - (PlayerSize / 2.0f);
    snap->Player1->ClientId = server_->ServerClientIds.at(0);
    snap->Player1->SizeX = PlayerSize;
    snap->Player1->SizeY = PlayerSize;
    snap->Player1->Lifes = 5;
    snap->Player2->PosX = WIDTH - PlayerSize;
    snap->Player2->PosY = (HEIGHT / 2.0f) - (PlayerSize / 2.0f);
    snap->Player2->ClientId = server_->ServerClientIds.at(1);
    snap->Player2->SizeX = PlayerSize;
    snap->Player2->SizeY = PlayerSize;
    snap->Player2->Lifes = 5;
  }
}

void MySimServer::ApplyInput(Input* input, Snapshot* snapshot,
                             bool clientEvents, Snapshot* rewindSnapshot) {
  CustomSnapshot* snap = dynamic_cast<CustomSnapshot*>(snapshot);

  switch (input->PacketId) {
    case KeyboardInputPacket::ID: {
      KeyboardInput* tempInput = dynamic_cast<KeyboardInput*>(input);
      shared_ptr<Player> player = nullptr;
      bool isPlayer1 = input->ClientId == snap->Player1->ClientId;
      if (input->ClientId == snap->Player1->ClientId) player = snap->Player1;
      if (input->ClientId == snap->Player2->ClientId) player = snap->Player2;

      switch (tempInput->Key) {
        case Direction::Up:
          player->SpeedY = tempInput->Down ? -PlayerSpeed : 0;
          break;
        case Direction::Down:
          player->SpeedY = tempInput->Down ? PlayerSpeed : 0;
          break;
        case Direction::Left:
          player->SpeedX = tempInput->Down ? -PlayerSpeed : 0;
          break;
        case Direction::Right:
          player->SpeedX = tempInput->Down ? PlayerSpeed : 0;
          break;
        case Direction::Shoot:
          if (tempInput->Down) {
            shared_ptr<Bullet> bullet = std::make_shared<Bullet>();
            bullet->ClientId = player->ClientId;
            bullet->NetId = GetNextNetId(tempInput->ClientId, snapshot);
            bullet->PosX =
                player->PosX + (isPlayer1 ? player->SizeX : -bullet->SizeX);
            bullet->PosY =
                player->PosY + ((player->SizeY / 2) - (bullet->SizeY / 2));
            bullet->SpeedX = isPlayer1 ? PlayerSpeed * 2 : -PlayerSpeed * 2;
            snap->Bullets.push_back(bullet);

            if (clientEvents) {
              // e.g. play sound
            }
          }
          break;
      }
    }
  }
}

// currently needed for client
void MySimServer::SendInput(shared_ptr<Input> input) {
  switch (input->PacketId) {
    case KeyboardInputPacket::ID: {
      auto packet = std::make_shared<KeyboardInputPacket>();
      packet->Input = std::dynamic_pointer_cast<KeyboardInput>(input);
      SendToServer(packet);
      break;
    }
  }
}

// Called by client and server to progress simulation in between inputs
void MySimServer::UpdateClientAndRelatedObjects(float delta, uint8_t clientId,
                                                bool clientEvents,
                                                polynet::Snapshot* snapshot,
                                                Snapshot* rewindSnapshot) {
  CustomSnapshot* snap = dynamic_cast<CustomSnapshot*>(snapshot);
  CustomSnapshot* rewindSnap =
      rewindSnapshot == nullptr ? nullptr
                                : dynamic_cast<CustomSnapshot*>(rewindSnapshot);
  Player* player = snap->Player1->ClientId == clientId ? snap->Player1.get()
                                                       : snap->Player2.get();
  Player* otherPlayer = snap->Player2->ClientId == clientId
                            ? snap->Player1.get()
                            : snap->Player2.get();

  player->PosX += delta * player->SpeedX;
  player->PosY += delta * player->SpeedY;

  if (player->PosY < 0) {
    player->PosY = 0;
  }

  if (player->PosY >= HEIGHT - PlayerSize) {
    player->PosY = HEIGHT - PlayerSize;
  }

  if (player->PosX < 0) {
    player->PosX = 0;
  }

  if (player->PosX >= WIDTH - PlayerSize) {
    player->PosX = WIDTH - PlayerSize;
  }

  // Update bullets
  for (auto& bullet : snap->Bullets) {
    if (bullet->ClientId == clientId) {
      bullet->PosX += delta * bullet->SpeedX;
      bullet->PosY += delta * bullet->SpeedY;
    }
  }

  // Delete bullets out of bounds
  snap->Bullets.erase(
      std::remove_if(snap->Bullets.begin(), snap->Bullets.end(),
                     [&](shared_ptr<Bullet>& bullet) {
                       if (bullet->ClientId != player->ClientId) return false;
                       return (bullet->PosX + bullet->SizeX < 0 ||
                               bullet->PosX > WIDTH);
                     }),
      snap->Bullets.end());

  snap->Bullets.erase(
      std::remove_if(
          snap->Bullets.begin(), snap->Bullets.end(),
          [&](shared_ptr<Bullet>& bullet) {
            if (bullet->ClientId != player->ClientId) return false;

            bool colliding = rectanglesOverlapping(
                otherPlayer->PosX, otherPlayer->PosY, otherPlayer->SizeX,
                otherPlayer->SizeY, bullet->PosX, bullet->PosY, bullet->SizeX,
                bullet->SizeY);

            if (colliding) {
              otherPlayer->Lifes--;

              if (rewindSnapshot != nullptr) {
                auto rewindPlayer = rewindSnap->Player2->ClientId == clientId
                                        ? rewindSnap->Player1.get()
                                        : rewindSnap->Player2.get();
                rewindPlayer->Lifes--;

                rewindSnap->Bullets.erase(
                    std::remove_if(rewindSnap->Bullets.begin(),
                                   rewindSnap->Bullets.end(),
                                   [&](shared_ptr<Bullet>& reBullet) {
                                     return reBullet->NetId == bullet->NetId;
                                   }),
                    rewindSnap->Bullets.end());
              }

              if (clientEvents) {
                // e.g. play sound
              }
            }

            return colliding;
          }),
      snap->Bullets.end());
}

vector<uint8_t> MySimServer::GetAllClientIds(polynet::Snapshot* snapshot) {
  CustomSnapshot* snap = dynamic_cast<CustomSnapshot*>(snapshot);
  vector<uint8_t> result;
  result.push_back(snap->Player1->ClientId);
  result.push_back(snap->Player2->ClientId);
  return result;
}

// Update world without client-specific code. e.g. updating some timers ...
void MySimServer::UpdateSnapshotWithoutClients(float delta, bool clientEvents,
                                               Snapshot* snapshot) {}

shared_ptr<Snapshot> MySimServer::CreateSnapshot() {
  return std::make_shared<CustomSnapshot>();
}

shared_ptr<Packet> MySimServer::CreateSnapshotPacket(
    shared_ptr<Snapshot> snapshot) {
  auto packet = std::make_shared<CustomSnapshotPacket>();
  packet->Snapshot = std::dynamic_pointer_cast<CustomSnapshot>(snapshot);
  return packet;
}

bool MySimServer::rectanglesOverlapping(float x1, float y1, float w1, float h1,
                                        float x2, float y2, float w2,
                                        float h2) {
  return (x1 < (x2 + w2) && (x1 + w1) > x2 && y1 < (y2 + h2) && (y1 + h1) > y2);
}
}  // namespace polynet_sample
