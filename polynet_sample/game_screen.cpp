/* Copyright (c) 2017 Tobias Holewa */

#include "game_screen.h"
#include "program.h"
#include "shared.h"

namespace polynet_sample {
const float SERVER_TICK_RATE = 1 / 60.0f;
const float MAX_PING_ALLOWED = 1.0f;
const float MAX_LIVE_INPUT_PING = 0.15f;
const float SIMULATION_FINE_TICK_RATE = 1.0f / 60;
const float NETWORK_CACHE_LENGTH = 0.05f;

sf::RectangleShape p1s, p2s;
sf::RectangleShape p21s, p22s;
sf::Font font;
sf::Text p11livesTxt;
sf::Text p22livesTxt;
sf::Text p12livesTxt;
sf::Text p21livesTxt;

bool pressedLastFrameP1[5] = {false};
bool pressedLastFrameP2[5] = {false};
bool pressedCurrentFrameP1[5] = {false};
bool pressedCurrentFrameP2[5] = {false};

GameScreen::GameScreen(Program* program) {
  // load font for printing
  font.loadFromFile("nokiafc22.ttf");

  this->currentGame_ = program;

  if (!polynet::PolyNet::Init()) {
    this->currentGame_->SetRunning(false);
  }

  server_ = make_unique<polynet::Server>(
      1203,
      std::move(unique_ptr<SimServer>(
          new MySimServer(true, SIMULATION_FINE_TICK_RATE))),
      SERVER_TICK_RATE, MAX_PING_ALLOWED, MAX_LIVE_INPUT_PING,
      NETWORK_CACHE_LENGTH);
  clientOne_ = make_unique<polynet::Client>(
      1203, "127.0.0.1",
      std::move(unique_ptr<SimServer>(
          new MySimServer(false, SIMULATION_FINE_TICK_RATE))),
      NETWORK_CACHE_LENGTH);
  // clientOne_->SetFakeLatency(0.05f);
  clientTwo_ = make_unique<polynet::Client>(
      1203, "127.0.0.1",
      std::move(unique_ptr<SimServer>(
          new MySimServer(false, SIMULATION_FINE_TICK_RATE))),
      NETWORK_CACHE_LENGTH);
  clientTwo_->SetFakeLatency(0.1f);
  clientOneSimServer_ = (MySimServer*)clientOne_->SimServerInstance.get();
  clientTwoSimServer_ = (MySimServer*)clientTwo_->SimServerInstance.get();

  server_->RunInSeperateThread(SERVER_TICK_RATE);

  p1s.setSize(sf::Vector2f(clientOneSimServer_->PlayerSize,
                           clientOneSimServer_->PlayerSize));
  p2s.setSize(sf::Vector2f(clientOneSimServer_->PlayerSize,
                           clientOneSimServer_->PlayerSize));
  p1s.setFillColor(sf::Color::Green);
  p2s.setFillColor(sf::Color::Green);

  p21s.setSize(sf::Vector2f(clientTwoSimServer_->PlayerSize,
                            clientTwoSimServer_->PlayerSize));
  p22s.setSize(sf::Vector2f(clientTwoSimServer_->PlayerSize,
                            clientTwoSimServer_->PlayerSize));
  p21s.setFillColor(sf::Color(0, 0, 255, 255));
  p22s.setFillColor(sf::Color::Blue);

  // setup text properties
  p11livesTxt.setFont(font);
  p11livesTxt.setCharacterSize(20);
  p11livesTxt.setFillColor(sf::Color::Green);
  p11livesTxt.setPosition(20, 20);

  p21livesTxt.setFont(font);
  p21livesTxt.setCharacterSize(20);
  p21livesTxt.setFillColor(sf::Color::Blue);
  p21livesTxt.setPosition(20, 60);

  p22livesTxt.setFont(font);
  p22livesTxt.setCharacterSize(20);
  p22livesTxt.setFillColor(sf::Color::Blue);
  p22livesTxt.setPosition(MySimServer::WIDTH - 116, 20);

  p12livesTxt.setFont(font);
  p12livesTxt.setCharacterSize(20);
  p12livesTxt.setFillColor(sf::Color::Green);
  p12livesTxt.setPosition(MySimServer::WIDTH - 116, 60);
}

GameScreen::~GameScreen() { polynet::PolyNet::Destroy(); }

void GameScreen::Update(float delta) {
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
    this->currentGame_->SetRunning(false);
    server_->StopSeperateThread();
  }

  // Input handling since SFML doesn't support key press
  pressedCurrentFrameP1[Direction::Up] =
      sf::Keyboard::isKeyPressed(sf::Keyboard::W);
  pressedCurrentFrameP1[Direction::Down] =
      sf::Keyboard::isKeyPressed(sf::Keyboard::S);
  pressedCurrentFrameP1[Direction::Left] =
      sf::Keyboard::isKeyPressed(sf::Keyboard::A);
  pressedCurrentFrameP1[Direction::Right] =
      sf::Keyboard::isKeyPressed(sf::Keyboard::D);
  pressedCurrentFrameP1[Direction::Shoot] =
      sf::Keyboard::isKeyPressed(sf::Keyboard::Space);

  pressedCurrentFrameP2[Direction::Up] =
      sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
  pressedCurrentFrameP2[Direction::Down] =
      sf::Keyboard::isKeyPressed(sf::Keyboard::Down);
  pressedCurrentFrameP2[Direction::Left] =
      sf::Keyboard::isKeyPressed(sf::Keyboard::Left);
  pressedCurrentFrameP2[Direction::Right] =
      sf::Keyboard::isKeyPressed(sf::Keyboard::Right);
  pressedCurrentFrameP2[Direction::Shoot] =
      sf::Keyboard::isKeyPressed(sf::Keyboard::Return);

  // player one keys pressed
  if (pressedCurrentFrameP1[Direction::Up] &&
      !pressedLastFrameP1[Direction::Up])
    clientOneSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Up, true));
  if (pressedCurrentFrameP1[Direction::Down] &&
      !pressedLastFrameP1[Direction::Down])
    clientOneSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Down, true));
  if (pressedCurrentFrameP1[Direction::Left] &&
      !pressedLastFrameP1[Direction::Left])
    clientOneSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Left, true));
  if (pressedCurrentFrameP1[Direction::Right] &&
      !pressedLastFrameP1[Direction::Right])
    clientOneSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Right, true));
  if (pressedCurrentFrameP1[Direction::Shoot] &&
      !pressedLastFrameP1[Direction::Shoot])
    clientOneSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Shoot, true));

  // player one keys released
  if (!pressedCurrentFrameP1[Direction::Up] &&
      pressedLastFrameP1[Direction::Up])
    clientOneSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Up, false));
  if (!pressedCurrentFrameP1[Direction::Down] &&
      pressedLastFrameP1[Direction::Down])
    clientOneSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Down, false));
  if (!pressedCurrentFrameP1[Direction::Left] &&
      pressedLastFrameP1[Direction::Left])
    clientOneSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Left, false));
  if (!pressedCurrentFrameP1[Direction::Right] &&
      pressedLastFrameP1[Direction::Right])
    clientOneSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Right, false));
  if (!pressedCurrentFrameP1[Direction::Shoot] &&
      pressedLastFrameP1[Direction::Shoot])
    clientOneSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Shoot, false));

  // player two keys pressed
  if (pressedCurrentFrameP2[Direction::Up] &&
      !pressedLastFrameP2[Direction::Up])
    clientTwoSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Up, true));
  if (pressedCurrentFrameP2[Direction::Down] &&
      !pressedLastFrameP2[Direction::Down])
    clientTwoSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Down, true));
  if (pressedCurrentFrameP2[Direction::Left] &&
      !pressedLastFrameP2[Direction::Left])
    clientTwoSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Left, true));
  if (pressedCurrentFrameP2[Direction::Right] &&
      !pressedLastFrameP2[Direction::Right])
    clientTwoSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Right, true));
  if (pressedCurrentFrameP2[Direction::Shoot] &&
      !pressedLastFrameP2[Direction::Shoot])
    clientTwoSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Shoot, true));

  // player two keys released
  if (!pressedCurrentFrameP2[Direction::Up] &&
      pressedLastFrameP2[Direction::Up])
    clientTwoSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Up, false));
  if (!pressedCurrentFrameP2[Direction::Down] &&
      pressedLastFrameP2[Direction::Down])
    clientTwoSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Down, false));
  if (!pressedCurrentFrameP2[Direction::Left] &&
      pressedLastFrameP2[Direction::Left])
    clientTwoSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Left, false));
  if (!pressedCurrentFrameP2[Direction::Right] &&
      pressedLastFrameP2[Direction::Right])
    clientTwoSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Right, false));
  if (!pressedCurrentFrameP2[Direction::Shoot] &&
      pressedLastFrameP2[Direction::Shoot])
    clientTwoSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Shoot, false));

  clientOneSimServer_->Update(delta);
  clientTwoSimServer_->Update(delta);

  pressedLastFrameP1[Direction::Up] = pressedCurrentFrameP1[Direction::Up];
  pressedLastFrameP1[Direction::Down] = pressedCurrentFrameP1[Direction::Down];
  pressedLastFrameP1[Direction::Left] = pressedCurrentFrameP1[Direction::Left];
  pressedLastFrameP1[Direction::Right] =
      pressedCurrentFrameP1[Direction::Right];
  pressedLastFrameP1[Direction::Shoot] =
      pressedCurrentFrameP1[Direction::Shoot];

  pressedLastFrameP2[Direction::Up] = pressedCurrentFrameP2[Direction::Up];
  pressedLastFrameP2[Direction::Down] = pressedCurrentFrameP2[Direction::Down];
  pressedLastFrameP2[Direction::Left] = pressedCurrentFrameP2[Direction::Left];
  pressedLastFrameP2[Direction::Right] =
      pressedCurrentFrameP2[Direction::Right];
  pressedLastFrameP2[Direction::Shoot] =
      pressedCurrentFrameP2[Direction::Shoot];
}

void GameScreen::Render(sf::RenderWindow* window) {
  window->clear(sf::Color::White);

  if (clientOneSimServer_->GetIsRunning() &&
      clientTwoSimServer_->GetIsRunning()) {
    auto snap1 = std::dynamic_pointer_cast<CustomSnapshot>(
        clientOneSimServer_->GetCurrentSnapshot());
    auto snap2 = std::dynamic_pointer_cast<CustomSnapshot>(
        clientTwoSimServer_->GetCurrentSnapshot());

    p1s.setPosition(sf::Vector2f(snap1->Player1->PosX, snap1->Player1->PosY));
    p2s.setPosition(sf::Vector2f(snap1->Player2->PosX, snap1->Player2->PosY));

    p21s.setPosition(sf::Vector2f(snap2->Player1->PosX, snap2->Player1->PosY));
    p22s.setPosition(sf::Vector2f(snap2->Player2->PosX, snap2->Player2->PosY));

    for (const auto& bullet : snap1->Bullets) {
      sf::RectangleShape bulletShape;
      bulletShape.setPosition(bullet->PosX, bullet->PosY);
      bulletShape.setSize(sf::Vector2f(bullet->SizeX, bullet->SizeY));
      bulletShape.setFillColor(sf::Color::Green);
      window->draw(bulletShape);
    }

    for (const auto& bullet : snap2->Bullets) {
      sf::RectangleShape bulletShape;
      bulletShape.setPosition(bullet->PosX, bullet->PosY);
      bulletShape.setSize(sf::Vector2f(bullet->SizeX, bullet->SizeY));
      bulletShape.setFillColor(sf::Color::Blue);
      window->draw(bulletShape);
    }

    p11livesTxt.setString("Lifes: " + std::to_string(snap1->Player1->Lifes));
    p21livesTxt.setString("Lifes: " + std::to_string(snap2->Player1->Lifes));
    p22livesTxt.setString("Lifes: " + std::to_string(snap2->Player2->Lifes));
    p12livesTxt.setString("Lifes: " + std::to_string(snap1->Player2->Lifes));
  }

  window->draw(p2s);
  window->draw(p21s);
  window->draw(p22s);
  window->draw(p1s);

  window->draw(p11livesTxt);
  window->draw(p22livesTxt);
  window->draw(p21livesTxt);
  window->draw(p12livesTxt);

  window->display();
}
}  // namespace polynet_sample
