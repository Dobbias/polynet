/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_MY_SIM_SERVER_H_
#define POLYNET_MY_SIM_SERVER_H_

#include <polynet/server_client.h>
#include <polynet/sim_server.h>
#include <cmath>
#include <iostream>

namespace polynet_sample {
using polynet::Input;
using polynet::Packet;
using polynet::ServerClient;
using polynet::SimServer;
using polynet::Snapshot;
using std::vector;

class MySimServer : public polynet::SimServer {
 public:
  // Dummy game Data
  static const int WIDTH = 1366;
  static const int HEIGHT = 768;
  const float PlayerSize = 40;
  const float PlayerSpeed = 200;

  MySimServer(bool serverMode, float fineDeltaBetweenInputs);
  void sOnClientConnected(ServerClient* serverClient) override;
  void OnPacketReceived(uint16_t packetId, uint8_t* data) override;
  void OnStart();

 protected:
  void UpdateSnapshotWithoutClients(float delta, bool clientEvents,
                                    Snapshot* snapshot) override;
  void UpdateClientAndRelatedObjects(float delta, uint8_t clientId,
                                     bool clientEvents, Snapshot* snapshot,
                                     Snapshot* rewindSnapshot) override;
  vector<uint8_t> GetAllClientIds(Snapshot* snapshot) override;
  void ApplyInput(Input* input, Snapshot* snapshot, bool clientEvents,
                  Snapshot* rewindSnapshot) override;
  void SendInput(shared_ptr<Input> input) override;
  shared_ptr<Snapshot> CreateSnapshot() override;
  shared_ptr<Packet> CreateSnapshotPacket(
      shared_ptr<Snapshot> snapshot) override;

 private:
  bool rectanglesOverlapping(float x1, float y1, float w1, float h1, float x2,
                             float y2, float w2, float h2);
};
}  // namespace polynet_sample

#endif  // POLYNET_MY_SIM_SERVER_H_
