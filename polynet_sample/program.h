/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_SAMPLE_PROGRAM_H_
#define POLYNET_SAMPLE_PROGRAM_H_

#include <SFML/Graphics.hpp>

#include <iostream>
#include <memory>

using std::unique_ptr;

int main(int, char**);

namespace polynet_sample {
class GameScreen;
class Program {
 public:
  Program();
  ~Program();
  void Run(Program* program);
  void SetRunning(bool state);

 private:
  bool running_;

  unique_ptr<sf::RenderWindow> window_;
  unique_ptr<GameScreen> currentScreen_;
};
}  // namespace polynet_sample

#endif  // POLYNET_SAMPLE_PROGRAM_H_
