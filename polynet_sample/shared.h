/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_SHARED_H
#define POLYNET_SHARED_H

#include <iostream>
#include <memory>

#include <polynet/polynet.h>

namespace polynet_sample {

using polynet::Packet;
using polynet::Snapshot;
using polynet::NetObject;
using polynet::Input;
using std::string;
using std::shared_ptr;
using std::static_pointer_cast;

enum Direction : uint8_t { Up = 0, Down = 1, Right = 2, Left = 3, Shoot = 4 };

class Bullet : public NetObject {
 public:
  float PosX, PosY, SizeX = 13, SizeY = 5, SpeedX, SpeedY;
  uint8_t ClientId;

  uint32_t GetNetSize() {
    // NetID short + rest
    return sizeof(uint16_t) + sizeof(uint8_t) + 6 * sizeof(float);
  }

  void Serialize(Packet* packet) {
    packet->s_short(NetId);
    packet->s_byte(ClientId);
    packet->s_float(PosX);
    packet->s_float(PosY);
    packet->s_float(SizeX);
    packet->s_float(SizeY);
    packet->s_float(SpeedX);
    packet->s_float(SpeedY);
  }

  void Deserialize(Packet* packet) {
    NetId = packet->d_short();
    ClientId = packet->d_byte();
    PosX = packet->d_float();
    PosY = packet->d_float();
    SizeX = packet->d_float();
    SizeY = packet->d_float();
    SpeedX = packet->d_float();
    SpeedY = packet->d_float();
  }

  shared_ptr<NetObject> Clone() {
    shared_ptr<Bullet> result = std::make_shared<Bullet>();
    result->NetId = NetId;
    result->ClientId = ClientId;
    result->PosX = PosX;
    result->PosY = PosY;
    result->SizeX = SizeX;
    result->SizeY = SizeY;
    result->SpeedX = SpeedX;
    result->SpeedY = SpeedY;
    return result;
  }
};

class Player : public NetObject {
 public:
  float PosX, PosY, SizeX, SizeY, SpeedX, SpeedY;
  uint8_t ClientId, Lifes;

  uint32_t GetNetSize() {
    // NetID short + rest
    return sizeof(uint16_t) + 2 * sizeof(uint8_t) + 6 * sizeof(float);
  }

  void Serialize(Packet* packet) {
    packet->s_short(NetId);
    packet->s_byte(ClientId);
    packet->s_byte(Lifes);
    packet->s_float(PosX);
    packet->s_float(PosY);
    packet->s_float(SizeX);
    packet->s_float(SizeY);
    packet->s_float(SpeedX);
    packet->s_float(SpeedY);
  }

  void Deserialize(Packet* packet) {
    NetId = packet->d_short();
    ClientId = packet->d_byte();
    Lifes = packet->d_byte();
    PosX = packet->d_float();
    PosY = packet->d_float();
    SizeX = packet->d_float();
    SizeY = packet->d_float();
    SpeedX = packet->d_float();
    SpeedY = packet->d_float();
  }

  shared_ptr<NetObject> Clone() {
    shared_ptr<Player> result = std::make_shared<Player>();
    result->NetId = NetId;
    result->ClientId = ClientId;
    result->Lifes = Lifes;
    result->PosX = PosX;
    result->PosY = PosY;
    result->SizeX = SizeX;
    result->SizeY = SizeY;
    result->SpeedX = SpeedX;
    result->SpeedY = SpeedY;
    return result;
  }
};

class KeyboardInput : public Input {
 public:
  char Key;
  bool Down;

  KeyboardInput(char key = -1, bool down = false)
      : Input(/*KeyboardInputPacket::ID*/ 1000) {
    this->Key = key;
    this->Down = down;
  }

  uint32_t GetNetSize() {
    return sizeof(uint8_t) + sizeof(float) + sizeof(uint8_t) + sizeof(uint8_t);
  }

  void Serialize(Packet* packet) {
    packet->s_byte(ClientId);
    packet->s_float(TimeStamp);
    packet->s_byte(Key);
    packet->s_bool(Down);
  }

  void Deserialize(Packet* packet) {
    ClientId = packet->d_byte();
    TimeStamp = packet->d_float();
    Key = packet->d_byte();
    Down = packet->d_bool();
  }

  shared_ptr<NetObject> Clone() {
    shared_ptr<KeyboardInput> result = std::make_shared<KeyboardInput>();
    result->ClientId = ClientId;
    result->TimeStamp = TimeStamp;
    result->Key = Key;
    result->Down = Down;
    return result;
  }
};

// own snapshot has to provide way to de/serialize inputs !!!
class CustomSnapshot : public Snapshot {
 public:
  std::shared_ptr<Player> Player1 = std::make_shared<Player>(),
                          Player2 = std::make_shared<Player>();
  std::vector<std::shared_ptr<Bullet>> Bullets;

  uint32_t GetNetSize() {
    // NetID short + rest
    int bulletCount = Bullets.size();
    int inputCount = Inputs.size();
    int netIdCount = NetIds.size();
    return sizeof(float) * 2 + Player1->GetNetSize() + Player2->GetNetSize() +
           sizeof(uint16_t) +
           (bulletCount > 0 ? bulletCount * Bullets[0]->GetNetSize() : 0) +
           sizeof(uint16_t) +
           (inputCount > 0 ? inputCount * Inputs[0]->GetNetSize() : 0) +
           sizeof(uint8_t) +
           (netIdCount > 0 ? netIdCount * (sizeof(uint8_t) + sizeof(uint32_t))
                           : 0);
  }

  void Serialize(Packet* packet) {
    packet->s_float(TimeStamp);
    packet->s_float(Duration);
    packet->s_byte(NetIds.size());
    for (auto& x : NetIds) {
      packet->s_byte(x.first);
      packet->s_int(x.second);
    }
    Player1->Serialize(packet);
    Player2->Serialize(packet);
    uint16_t bulletSize = Bullets.size();
    packet->s_short(Bullets.size());
    for (auto& x : Bullets) {
      x->Serialize(packet);
    }
    packet->s_short(Inputs.size());
    for (auto& x : Inputs) {
      x->Serialize(packet);
    }
  }

  void Deserialize(Packet* packet) {
    TimeStamp = packet->d_float();
    Duration = packet->d_float();
    uint8_t idCount = packet->d_byte();
    for (int i = 0; i < idCount; ++i) {
      NetIds.insert(std::make_pair(packet->d_byte(), packet->d_int()));
    }
    Player1->Deserialize(packet);
    Player2->Deserialize(packet);
    uint16_t bulletCount = packet->d_short();
    for (int i = 0; i < bulletCount; ++i) {
      auto temp = std::make_shared<Bullet>();
      temp->Deserialize(packet);
      Bullets.push_back(temp);
    }
    uint16_t inputCount = packet->d_short();
    for (int i = 0; i < inputCount; ++i) {
      auto temp = std::make_shared<KeyboardInput>();
      temp->Deserialize(packet);
      Inputs.push_back(temp);
    }
  }

  shared_ptr<NetObject> Clone() {
    shared_ptr<CustomSnapshot> result = std::make_shared<CustomSnapshot>();
    result->TimeStamp = TimeStamp;
    result->Duration = Duration;
    result->NetIds = NetIds;
    result->Player1 = static_pointer_cast<Player>(Player1->Clone());
    result->Player2 = static_pointer_cast<Player>(Player2->Clone());
    for (auto& x : Bullets) {
      result->Bullets.push_back(static_pointer_cast<Bullet>(x->Clone()));
    }
    for (auto& x : Inputs) {
      result->Inputs.push_back(static_pointer_cast<KeyboardInput>(x->Clone()));
    }
    return result;
  }
};

class KeyboardInputPacket : public Packet {
 public:
  static const uint16_t ID = 1000;

  shared_ptr<KeyboardInput> Input = std::make_shared<KeyboardInput>();

  KeyboardInputPacket() : Packet(ID) {}

  uint8_t* Serialize(int& outSize) {
    uint8_t* outData = getBuffer(Input->GetNetSize(), outSize);
    Input->Serialize(this);
    return outData;
  }

  void Deserialize(uint8_t* data) {
    setBuffer(data);
    Input->Deserialize(this);
  }
};

class CustomSnapshotPacket : public Packet {
 public:
  static const uint16_t ID = 1001;

  shared_ptr<CustomSnapshot> Snapshot;
  CustomSnapshotPacket() : Packet(ID) {
    Snapshot = std::make_shared<CustomSnapshot>();
  };

  uint8_t* Serialize(int& outSize) {
    uint8_t* outData = getBuffer(Snapshot->GetNetSize(), outSize);
    Snapshot->Serialize(this);
    return outData;
  }

  void Deserialize(uint8_t* data) {
    setBuffer(data);
    Snapshot->Deserialize(this);
  }
};
}  // namespace polynet_sample

#endif  // POLYNET_SHARED_H
