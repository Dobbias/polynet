/* Copyright (c) 2017 Tobias Holewa */

#include "program.h"
#include "game_screen.h"
#include "my_sim_server.h"

using std::make_unique;

namespace polynet_sample {
Program::Program() {
  window_ = make_unique<sf::RenderWindow>(
      sf::VideoMode(MySimServer::WIDTH, MySimServer::HEIGHT), "PolyNet Sample",
      sf::Style::Close);
  window_->setVerticalSyncEnabled(true);
}

Program::~Program() {}

void Program::Run(Program* program) {
  std::cout << "Running gameloop . . ." << std::endl;
  currentScreen_ = make_unique<GameScreen>(program);

  running_ = true;
  sf::Event event;

  sf::Clock clock;
  sf::Time elapsed;

  while (running_) {
    while (window_->pollEvent(event)) {
      if (event.type == sf::Event::Closed) running_ = false;
    }

    elapsed = clock.restart();

    currentScreen_->Update(elapsed.asSeconds());
    currentScreen_->Render(window_.get());
  }

  window_->close();
}

void Program::SetRunning(bool state) { running_ = state; }
}  // namespace polynet_sample

int main(int argc, char** argv) {
  unique_ptr<polynet_sample::Program> sample =
      make_unique<polynet_sample::Program>();
  sample->Run(sample.get());
  return 0;
}
