/* Copyright (c) 2017 Tobias Holewa */

#ifndef POLYNET_SAMPLE_GAME_SCREEN_H_
#define POLYNET_SAMPLE_GAME_SCREEN_H_

#include <polynet/polynet.h>
#include <SFML/Graphics.hpp>

#include <memory>
#include "my_sim_server.h"

using std::shared_ptr;
using std::unique_ptr;
using std::weak_ptr;
using std::make_shared;
using std::make_unique;

namespace polynet_sample {
class Program;

class GameScreen {
 public:
  explicit GameScreen(Program* program);
  ~GameScreen();
  void Update(float delta);
  void Render(sf::RenderWindow* renderer);

 private:
  unique_ptr<polynet::Client> clientOne_;
  unique_ptr<polynet::Client> clientTwo_;
  unique_ptr<polynet::Server> server_;
  Program* currentGame_;
  MySimServer* clientOneSimServer_;
  MySimServer* clientTwoSimServer_;
};
}  // namespace polynet_sample

#endif  // POLYNET_SAMPLE_GAME_SCREEN_H_
